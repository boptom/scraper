# Scraper

Scraper is a general purpose text parser designed to scrape malformed HTML.

## Quickstart Examples

The **Scraper** class manipulates the `$text` property via chainable methods.

```php
$scraper = new Scraper($html);
$link = $scraper->getPattern([ '<a herf="', '"' ])->getText();
```

**ScraperHelpers** static classes provide the same functions.

```php
$link = ScraperHelpers::getPattern([ '<a herf="', '"' ], $html);
```
