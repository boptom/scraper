<?php

abstract class TestCase extends PHPUnit\Framework\TestCase
{
    protected $html;

    protected function setUp() : void
    {
        $this->html = file_get_contents(__DIR__.'/test.html');
    }
}
