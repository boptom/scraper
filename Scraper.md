# class Scraper functions

Most methods return `$this` which allows chaining. Those which don't return `$this` will be marked with an asterisk (*).

Most methods are used to manipulate the property `$text`.
_Note: `$text` can also be an array._


## *__construct($text = '')

Pass through the `$text` to parse in the constructor.

```php
$scraper = new Scraper($html);
```

## setText($text)

Sets the `$text` to parse.

```php
$scraper->setText($html);
```

## *getText()

Gets the parsed `$text`.

```php
echo $scraper->getText();
```

## *asNumeric()

Returns `$text` if numeric. Otherwise zero. Useful for adding up scraped values which may not be in the text to parse.

```php
echo $scraper->asNumeric();
```

## *getType()

Returns the type of `$text`.

```php
$type = $scraper->getType();
```

## *isArray()

Returns `true` if `$text` is an array. Otherwise `false`.

```php
$scraper->isArray();
```

## *isString()

Returns `true` if `$text` is a string. Otherwise `false`.

```php
$scraper->isString();
```

## *toString($glue = null)

**If `$text` is an array**: returns `$text` as a json encoded string or if `$glue` is passed, implodes the array.

Otherwise, returns `$text`

```php
$scraper = new Scraper([  'name' => 'john',  'email' => 'john@example.com' ]);

echo $scraper->toString();

// {"name":"john","email":"john@example.com"}

echo $scraper->toString("|");

// john|john@example.com
```

## getPattern(array $pattern, $start = 0)

Finds a pattern in a string and returns the string between the 2nd last and last match.
An array of patterns can also be passed (returns an array of strings).

`$start` sets the starting point.

If no valid matches are found `$text` is set to a blank string.

```php
$scraper = new Scraper("this is a short sentence");
$output = $scraper->getPattern([ 'is a ', ' sentence' ])->getText();

// short
```

```php
$scraper = new Scraper("this is a short sentence");
$output = $scraper->getPattern([
  [ 'is a ', ' sentence' ],
  [ 'this ', ' short' ]
])->getText();

// [ "short", "is a" ]
```

## getPatternRepeat(array $pattern, $start = 0)

Repeatedly finds a pattern in a string and returns the string between the 2nd last and last match; as an **array of strings**.

As above, an array of patterns can also be passed.

`$start` sets the starting point.

If no valid matches are found `$text` is set to a blank string.

```php
$scraper = new Scraper("<li>first</li><li>second</li>");
$output = $scraper->getPatternRepeat([ '<li>', '</li>' ])->getText();

// [ "first", "second" ]
```

## getDiv($seed, $start = 0, $inclusive = false)

Finds the string between the **first** `<div></div>` or other `<*></*>` tag.

`$start` position to start searching.
`$inclusive` set to true to include the tags.

```php
$scraper = new Scraper("<li class='list-class'>first</li><li>second</li>");
$output = $scraper->getDiv("list-class")->getText();

// first
```

## getDivRepeat(string $seed, $start = 0, $inclusive = false)

Finds the string between the **first** `<div></div>` or other `<*></*>` tag, repeatedly until the end of `$text`.

`$start` position to start searching.
`$inclusive` set to true to include the tags.

```php
$scraper = new Scraper("<li class='list-class'>first</li><li class='list-class'>second</li>");
$output = $scraper->getDivRepeat("list-class")->getText();

// [ "first", "second" ]
```

## getValue(string $name)

Gets value from `<input name='$name' value='...'>` tags.

```php
$scraper = new Scraper("<input name='amount' value='12'>");
$output = $scraper->getValue("amount")->getText();

// 12
```

## getPostFields($includeCheckboxes = false)

Finds postfields within `$text` as array in format `'name' => 'value'`.

`$includeCheckboxes` to true to include checkbox values.

```php
$scraper = new Scraper("<input name='amount' value='12'>");
$output = $scraper->getPostFields("amount")->getText();

// [ 'amount' => '12' ]
```

## jsonEncode()

Applies `json_encode` to `$text`.

```php
$scraper->jsonEncode();
```

## implode($glue = ',')

Applies `implode` to `$text`.

```php
$scraper->implode();
```

## replace(string $search, string $replace)

Applies `str_replace` to `$text`.
If `$text` is an array, will apply `str_replace` to all values of the array.

```php
$scraper->replace("this", "that");
```

## unique()

Keeps only unique items in `$text`. Only useful is `$text` is an array, otherwise will return `$text`.

```php
$scraper = new Scraper([ 1, 1, 2, 3 ]);
$unique = $scraper->unique()->getText();

// [ 1, 2, 3 ]
```

## filter(callable $callback = null)

Runs a filter over `$text`. A callback can be passed, otherwise will simply use `array_filter`.

```php
$scraper = new Scraper([ 0, 1, 1, 2, 3 ]);
$output = $scraper->filter()->getText();

// [ 1, 1, 2, 3 ]
```

## removeEmpty()

Removes empty items from `$text`.
See http://php.net/manual/en/function.empty.php for definition of empty.

```php
$scraper = new Scraper([ 0, 1, 2, 3, '', null, false ]);
$output =$scraper->removeEmpty()->getText();

// [ 1, 2, 3 ]
```

## except($list)

Keeps all items except those in `$list`.

```php
$scraper = new Scraper([ 0, 1, 2, 3 ]);
$output = $scraper->except([ 1, 3 ])->getText();

// [ 0, 2 ]
```

## sort($sortFlags = SORT_REGULAR)

Sorts `$text`.
See http://php.net/manual/en/function.sort.php for sort flags.

```php
$scraper = new Scraper([ 5, 12, -1, 6 ]);
$output = $scraper->sort()->getText();

// [ -1, 5, 6, 12 ]
```

## reverseSort($sortFlags = SORT_REGULAR)

Sorts `$text` in reverse order.
See http://php.net/manual/en/function.rsort.php for sort flags.

```php
$scraper = new Scraper([ 5, 12, -1, 6 ]);
$output = $scraper->sort()->getText();

// [ 12, 6, 5, -1 ]
```

## removeBetween(string $from, string $to)

Removes all sections of a string (or array of strings) between two parts, inclusive of `$from` and `$to`.

```php
$scraper = new Scraper("my dog is the greatest ever");
$output = $scraper->removeBetween(" dog", "greatest")->getText();

// my ever
```

## removeNonNumericItems()

Removes non-numeric items from `$text`.

```php
$scraper = new Scraper([ 1, 2, "word" ]);
$output = $scraper->removeNonNumericItems()->getText();

// [ 1, 2 ]
```

## removeNonNumericCharacters($keepDecimalPoint = false)

Removes non-numeric characters from a string or array of strings.
Set `$keepDecimalPoint` to true to ignore decimal point.

```php
$scraper = new Scraper("price = $14.50");
$output = $scraper->removeNonNumericCharacters()->getText();

// 1450
```

## removeBlankStrings()

Removes blank strings from an `(array) $text`.

```php
$scraper = new Scraper([ 1, "two", "", "", "three" ]);
$output = $scraper->removeBlankStrings()->getText();

// [ 1, "two", "three" ]
```

## trimStripTags()

`trim` then `strip_tags` from `$text`.

```php
$scraper = new Scraper("  <title>The page title</title>  ");
$output = $scraper->trimStripTags()->getText();

// The page title
```

## trim()

Applies `trim` to `$text`.

```php
$scraper = new Scraper("  <title>The page title</title>  ");
$output = $scraper->trim()->getText();

// <title>The page title</title>
```

## stripTags()

Applies `strip_tags` to `$text`.

```php
$scraper = new Scraper("<title>The page title</title>");
$output = $scraper->stripTags()->getText();

// The page title
```

## urlDecode()

Applies urldecode to `$text`.

```php
$scraper = new Scraper("myvar=one%26two");
$output = $scraper->urlDecode()->getText();

// myvar=one&two
```

## first()

Finds first of `(array) $text`.

```php
$scraper = new Scraper([ 0, false, "", 10, 20 ]);
$output = $scraper->first()->getText();

// 0
```

## last()

Finds last of `(array) $text`.

```php
$scraper = new Scraper([ 0, false, "", 10, 20 ]);
$output = $scraper->last()->getText();

// 20
```

## firstOf()

Finds first non empty, non-zero, non-false value of `(array) $text`, or return false if all empty.

```php
$scraper = new Scraper([ 0, false, "", 10, 20 ]);
$output = $scraper->firstOf()->getText();

// 10
```

## firstNonBlank()

Finds first non blank value of `(array) $text`, or false if all blank.

```php
$scraper = new Scraper([ "", "", "first", "second" ]);
$output = $scraper->firstNonBlank()->getText();

// first
```

## firstNumeric()

Finds first numeric value of an `(array) $text`, or `false` if all non-numeric.

```php
$scraper = new Scraper([ "abc", 1, 2, 3 ]);
$output = $scraper->firstNumeric()->getText();

// 1
```

## shorterOf(), shortest()

Returns the shortest string in `(array) $text`.

```php
$scraper = new Scraper([ "super long", "short", "longer" ]);
$output = $scraper->shorterOf()->getText();

// short
```

## getFirstNumberPosition($start = 0)

Finds the **position** of the first number in `$text`, beginning at `$start`, or `false` if there are no numbers in the string.


```php
$scraper = new Scraper("There are 3 cups in the drawer");
$output = $scraper->getFirstNumberPosition()->getText();

// 10
```

## removeInlineCss()

Removes all text between `<style` and `/style>`

```php
$scraper = new Scraper("<style>color: #red;</style>This is some text");
$output = $scraper->removeInlineCss()->getText();

// This is some text
```

## removeInlineJavascript()

Removes all text between `<script` and `/script>`

```php
$scraper = new Scraper("<script>alert(1)</script>This is some text");
$output = $scraper->removeInlineJavascript()->getText();

// This is some text
```

## removeHtmlComments()

Removes all text between `<!--` and `-->`

```php
$scraper = new Scraper("<!-- comments -->This is some text");
$output = $scraper->removeInlineComments()->getText();

// This is some text
```

## compressHtml()

Removes blank spaces and newlines between html tags.

```php
$scraper = new Scraper("<div>  <p> Hello    </p>  </div>");
$output = $scraper->compressHtml()->getText();

// <div><p>Hello</p></div>
```

## append($suffix)

Appends a string `$suffix` to `$text`.

If `$text` is an array, appends `$suffix` to all elements within the array.

```php
$scraper = new Scraper("Oranges are ");
$output = $scraper->append("yummy")->getText();

// Oranges are yummy
```

## prepend($prefix)

Prepends a string `$prefix` to `$text`.

If `$text` is an array, prepends `$prefix` to all elements within the array.

```php
$scraper = new Scraper(" are sour");
$output = $scraper->prepend("Lemons")->getText();

// Lemons are sour
```

## arrayMerge(...$arrays)

Merges the `...$arrays` into `$text`.

```php
$scraper = new Scraper("one");
$output = $scraper->arrayMerge([ "two", "three" ])->getText();

// [ "one", "two", "three" ]
```
