# ScraperHelpers

**ScraperHelpers** are a set of static methods which mimic those in the **Scraper** class.

## getPattern(array $pattern, string $text, $start = 0)

Finds a pattern in a string and returns the string between the 2nd last and last match.
An array of patterns can also be passed (returns an array of strings).

`$start` sets the starting point.

Returns a blank string If no valid matches are found.

```php
$output = ScraperHelpers::getPattern([ 'is a ', ' sentence' ], "this is a short sentence");

// short
```

## getPatternRepeat(array $pattern, string $text, $start = 0)

Repeatedly finds a pattern in a string and returns the string between the 2nd last and last match; as an **array of strings**.

As above, an array of patterns can also be passed.

`$start` sets the starting point.

Returns a blank string If no valid matches are found.

```php
$output = ScraperHelpers::getPatternRepeat([ '<li>', '</li>' ], "<li>first</li><li>second</li>");

// [ "first", "second" ]
```

## getDiv($seed, string $html, $start = 0, $inclusive = false)

Finds the string between the **first** `<div></div>` or other `<*></*>` tag.

`$start` position to start searching.
`$inclusive` set to true to include the tags.

```php
$output = ScraperHelpers::getDiv("list-class", "<li class='list-class'>first</li><li>second</li>");

// first
```

## getDivRepeat(string $seed, string $html, $start = 0, $inclusive = false)

Finds the string between the **first** `<div></div>` or other `<*></*>` tag, repeatedly until the end of `$html`.

`$start` position to start searching.
`$inclusive` set to true to include the tags.

```php
$output = ScraperHelpers::getDivRepeat("list-class", "<li class='list-class'>first</li><li class='list-class'>second</li>");

// [ "first", "second" ]
```

## getValue(string $name, string $html)

Gets value from `<input name='$name' value='...'>` tags.

```php
$output = ScraperHelpers::getValue("amount", "<input name='amount' value='12'>");

// 12
```

## getPostFields(string $html, $includeCheckboxes = false)

Returns postfields within `$html` as array in format `'name' => 'value'`.

`$includeCheckboxes` to true to include checkbox values.

```php
$output = ScraperHelpers::getPostFields("amount", "<input name='amount' value='12'>");

// [ 'amount' => '12' ]
```

## asNumeric($value)

Returns `$value` if numeric. Otherwise zero. Useful for adding up scraped values which may not be in the text to parse.

```php
echo ScraperHelpers::asNumeric("501");

// 501

echo ScraperHelpers::asNumeric("some text");

// 0
```

## removeBetween(string $from, string $to, $text)

Removes all sections of a string (or array of strings) between two parts, inclusive of `$from` and `$to`.

```php
$output = ScraperHelpers::removeBetween(" dog", "greatest", "my dog is the greatest ever");

// my ever
```

## removeNonNumericCharacters($text, $keepDecimalPoint = false)

Removes non-numeric characters from a string or array of strings.
Set `$keepDecimalPoint` to true to ignore decimal point.

```php
$output = ScraperHelpers::removeNonNumericCharacters("price = $14.50");

// 1450
```

## removeInlineCss($html)

Removes all text between `<style` and `/style>`

```php
$output = ScraperHelpers::removeInlineCss("<style>color: #red;</style>This is some text");

// This is some text
```

## removeInlineJavascript($html)

Removes all text between `<script` and `/script>`

```php
$output = ScraperHelpers::removeInlineJavascript("<script>alert(1)</script>This is some text");

// This is some text
```

## removeHtmlComments($html)

Removes all text between `<!--` and `-->`

```php
$output = ScraperHelpers::removeInlineComments(("<!-- comments -->This is some text");

// This is some text
```

## removeEmpty($array)

Removes empty items from `$array`.
See http://php.net/manual/en/function.empty.php for definition of empty.

```php
$output =ScraperHelpers::removeEmpty([ 0, 1, 2, 3, '', null, false ]);

// [ 1, 2, 3 ]
```

## removeBlankStrings($array)

Removes blank strings from an `$array`.

```php
$output = ScraperHelpers::removeBlankStrings([ 1, "two", "", "", "three" ]);

// [ 1, "two", "three" ]
```

## trim($text)

Applies `trim` to `$text`.

```php
$output = ScraperHelpers::trim("  <title>The page title</title>  ");

// <title>The page title</title>
```

## stripTags($text)

Applies `strip_tags` to `$text`.

```php
$output = ScraperHelprs::stripTags("<title>The page title</title>");

// The page title
```

## trimStripTags($text)

`trim` then `strip_tags` from `$text`.

```php
$output = ScraperHelpers::trimStripTags("  <title>The page title</title>  ");

// The page title
```

## urlDecode($text)

Applies urldecode to `$text`.

```php
$output = ScraperHelpers::urlDecode("myvar=one%26two");

// myvar=one&two
```

## compressHtml($html)

Removes blank spaces and newlines between html tags.

```php
$output = ScraperHelpers::compressHtml("<div>  <p> Hello    </p>  </div>");

// <div><p>Hello</p></div>
```

## firstOf($values)

Finds first non empty, non-zero, non-false value of `(array) $values`, or return false if all empty.

```php
$output = ScraperHelpers::firstOf([ 0, false, "", 10, 20 ]);

// 10
```

## firstNonBlank($values)

Finds first non blank value of `(array) $values`, or false if all blank.

```php
$output = ScraperHelpers::firstNonBlank([ "", "", "first", "second" ]);

// first
```

## firstNumeric($values)

Finds first numeric value of an `(array) $values`, or `false` if all non-numeric.

```php
$output = ScraperHelpers::firstNumeric([ "abc", 1, 2, 3 ]);

// 1
```

## shorterOf($texts)

Returns the shortest string in `(array) $texts`.

```php
$output = ScraperHelpers::shorterOf([ "super long", "short", "longer" ]);

// short
```

## getFirstNumberPosition(string $text, $start = 0)

Finds the **position** of the first number in `$text`, beginning at `$start`, or `false` if there are no numbers in the string.


```php
$output = ScraperHelpers::getFirstNumberPosition("There are 3 cups in the drawer");

// 10
```

## nextPosition($needle, $haystack, $start = 0)
Returns next position of `$needle` within a `$haystack`.
Returns the length of `$haystack` if not found.

```php
$output = ScraperHelpers::nextPosition("people", "There are people on tv");

// 10
```

## previousPosition($needle, $haystack, $start = null)
Returns previous (or last) position of `$needle` within a `$haystack`.
Returns -1 if not found.

```php
$output = ScraperHelpers::previousPosition("tv", "The tv is brown. There are people on tv");

// 37
```
