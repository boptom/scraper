<?php

use Scraper\Scraper;
use Scraper\scraperHelpers;

class ScraperTest extends TestCase
{
    protected $scraper;

    protected function setUp() : void
    {
        parent::setUp();
        $this->scraper = new Scraper($this->html);
    }

    public function test_setText_and_getText()
    {
        $text = $this->scraper->setText('new text')->getText();
        $this->assertEquals('new text', $text);
    }

    public function test_getType()
    {
        $type = $this->scraper->setText(123)->getType();
        $this->assertEquals('integer', $type);

        $type = $this->scraper->setText('some text')->getType();
        $this->assertEquals('string', $type);
    }

    public function test_toString()
    {
        $text = $this->scraper->setText(123)->toString();
        $this->assertEquals(123, $text);

        $text = $this->scraper->setText('a string')->toString();
        $this->assertEquals('a string', $text);

        $text = $this->scraper->setText([ 'first', 'second', 'third' ])->toString();
        $this->assertEquals('["first","second","third"]', $text);

        $text = $this->scraper->setText([ 'first', 'second', 'third' ])->toString(',');

        $this->assertEquals('first,second,third', $text);
    }

    public function test_jsonEncode()
    {
        $text = $this->scraper->setText([ 'first', 'second', 'third' ])->jsonEncode()->getText();
        $this->assertEquals('["first","second","third"]', $text);

        $text = $this->scraper->setText('a string')->jsonEncode()->getText();
        $this->assertEquals('"a string"', $text);
    }

    public function test_implode()
    {
        $text = $this->scraper->setText([ 'first', 'second', 'third' ])->implode()->getText();
        $this->assertEquals('first,second,third', $text);

        $text = $this->scraper->setText([ 'first', 'second', 'third' ])->implode('|')->getText();
        $this->assertEquals('first|second|third', $text);

        $text = $this->scraper->setText('a string')->implode()->getText();
        $this->assertEquals('a string', $text);
    }

    public function test_replace()
    {
        $text = $this->scraper->setText('the toy pony is blue')->replace('pony', 'horse')->getText();
        $this->assertEquals('the toy horse is blue', $text);

        $text = $this->scraper->setText('blue blue blue')->replace('blue', 'red')->getText();
        $this->assertEquals('red red red', $text);

        $text = $this->scraper
                     ->setText([ 'first string', 'second string', 'third string' ])
                     ->replace('string', 'thing')
                     ->getText();

        $this->assertSame([ 'first thing', 'second thing', 'third thing' ], $text);
    }

    public function test_unique()
    {
        $text = $this->scraper->setText('an example string')->unique()->getText();
        $this->assertEquals('an example string', $text);

        $text = $this->scraper->setText([ 5, 2, 3, 4, 5 ])->unique()->getText();
        $this->assertEquals([ 5, 2, 3, 4 ], $text);
    }

    public function test_removeEmpty()
    {
        $text = $this->scraper->setText('an example string')->removeEmpty()->getText();
        $this->assertEquals('an example string', $text);

        $text = $this->scraper->setText([ 0, null, false, 'abc', 1, -2, '' ])->removeEmpty()->getText();
        $this->assertEquals([ 'abc', 1, -2 ], $text);
    }

    public function test_except()
    {
        $text = $this->scraper->setText('a string')->except('some string')->getText();
        $this->assertEquals('a string', $text);

        $text = $this->scraper->setText('a string')->except('a string')->getText();
        $this->assertEquals('', $text);

        $text = $this->scraper->setText('a string')->except([ 'a string', 'some string' ])->getText();
        $this->assertEquals('', $text);

        $text = $this->scraper->setText([ 1, 2, 3 ])->except(1)->getText();
        $this->assertEquals([ 2, 3 ], $text);

        $text = $this->scraper->setText([ 1, 2, 3 ])->except([ 2, 3 ])->getText();
        $this->assertEquals([ 1 ], $text);
    }

    public function test_filter()
    {
        $text = $this->scraper->setText('a string')->filter('is_numeric')->getText();
        $this->assertEquals('', $text);

        $text = $this->scraper->setText(123)->filter('is_numeric')->getText();
        $this->assertEquals(123, $text);

        $text = $this->scraper->setText([ 123, 'abc' ])->filter('is_numeric')->getText();
        $this->assertSame([ 123 ], $text);
    }

    public function test_sort()
    {
        $text = $this->scraper->setText([ 4, 10, -0.5, 100 ])->sort()->getText();
        $this->assertSame([ -0.5, 4, 10, 100 ], $text);

        $text = $this->scraper->setText([ 'a', 'aardvark', 'zoo', 'cat' ])->sort()->getText();
        $this->assertSame([ 'a', 'aardvark', 'cat', 'zoo' ], $text);
    }

    public function test_reverseSort()
    {
        $text = $this->scraper->setText([ 4, 10, -0.5, 100 ])->reverseSort()->getText();
        $this->assertSame([ 100, 10, 4, -0.5 ], $text);

        $text = $this->scraper->setText([ 'a', 'aardvark', 'zoo', 'cat' ])->reverseSort()->getText();
        $this->assertSame([ 'zoo', 'cat', 'aardvark', 'a' ], $text);
    }

    public function test_removeNonNumericItems()
    {
        $text = $this->scraper->setText('a string')->removeNonNumericItems()->getText();
        $this->assertSame('', $text);

        $text = $this->scraper->setText(123)->removeNonNumericItems()->getText();
        $this->assertSame(123, $text);

        $text = $this->scraper->setText('123')->removeNonNumericItems()->getText();
        $this->assertSame('123', $text);

        $text = $this->scraper->setText([ 'abc', '123', 0.567 ])->removeNonNumericItems()->getText();
        $this->assertSame([ '123', 0.567 ], $text);
    }

    public function test_getPattern()
    {
        $text = $this->scraper
                     ->setText($this->html)
                     ->getPattern([
                            [ '<html>', '<title>', '</title>' ],
                            [ '<table', '<td>', '</td>' ],
                        ])
                     ->getText();

        $this->assertSame([
            'Test File',
            'Row1, Data1',
        ], $text);

        $text = $this->scraper
                     ->setText($this->html)
                     ->getPattern([])
                     ->getText();

        $this->assertEquals('', $text);

        $text = $this->scraper
                     ->setText($this->html)
                     ->getPattern([ '<head>' ])
                     ->getText();

        $this->assertEquals('<html>', $text);

        $text = $this->scraper
                     ->setText($this->html)
                     ->getPattern([ '<html>', '<title>', '</title>' ])
                     ->getText();

        $this->assertEquals('Test File', $text);

        $text = $this->scraper
                     ->setText($this->html)
                     ->getPattern([ '<html>', '<title>', '</title>' ], 100)
                     ->getText();

        $this->assertEquals('', $text);

        $text = $this->scraper
                     ->setText($this->html)
                     ->getPattern([ '<table', '<td>', '</td>' ])
                     ->getText();

        $this->assertEquals('Row1, Data1', $text);

        $text = $this->scraper
                     ->setText($this->html)
                     ->getPattern([ '<td>', '</td>' ], 600)
                     ->getText();

        $this->assertEquals('Row2, Data1', $text);

        $text = $this->scraper
                     ->setText($this->html)
                     ->getPattern([ '<html>', '<title>', '</title>' ], 'non-numeric')
                     ->getText();

        $this->assertEquals('Test File', $text);
    }

    public function test_getPatternRepeat()
    {
        $values = $this->scraper->setText($this->html)->getPatternRepeat([ '<th>', '</th>' ])->getText();
        $this->assertSame([
            'First',
            'Second',
            'Third',
        ], $values);

        $values = $this->scraper->setText($this->html)->getPatternRepeat([
            [ '<th>', '</th>' ],
            [ '<li>', '</li>' ],
        ])->getText();

        $this->assertSame([
            [ 'First', 'Second', 'Third' ],
            [ 'list 1', 'list 2', 'list 3', 'list 4' ],
        ], $values);
    }

    public function test_getFirstNumberPosition()
    {
        $position = $this->scraper->setText('abcd1234')->getFirstNumberPosition()->getText();
        $this->assertEquals(4, $position);

        $position = $this->scraper->setText('a1bcd1234')->getFirstNumberPosition(4)->getText();
        $this->assertEquals(5, $position);
    }

    public function test_removeBetween()
    {
        $text = $this->scraper
                     ->setText('1/234"abcd-zz0/23lkjh"ab098')
                     ->removeBetween('/23', '"ab')
                     ->getText();

        $this->assertEquals('1cd-zz0098', $text);

        $text = $this->scraper
                     ->setText([
                        '1234abcd-zz023lkjhab098',
                        'j23kladj*):"dsabkajkljab24h',
                     ])
                     ->removeBetween('23', 'ab')
                     ->getText();

        $this->assertSame([
            '1cd-zz0098',
            'jkajkljab24h',
        ], $text);
    }

    public function test_removeNonNumericCharacters()
    {
        $text = $this->scraper
                     ->setText('21jk0h213-.sdl*7# ')
                     ->removeNonNumericCharacters()
                     ->getText();

        $this->assertEquals('2102137', $text);

        $text = $this->scraper
                     ->setText('21jk0h213-.sdl*7# ')
                     ->removeNonNumericCharacters(true)
                     ->getText();

        $this->assertEquals('210213.7', $text);

        $texts = $this->scraper
                      ->setText([
                        'j3hkfh3#F=8H+,D*',
                        'Dasdj0kd 4 AD2a8.&j',
                        '92fsG:17"39\80/08 9?',
                      ])
                      ->removeNonNumericCharacters()
                      ->getText();

        $this->assertSame([
            '338',
            '0428',
            '92173980089',
        ], $texts);
    }

    public function test_trimStripTags()
    {
        $text = $this->scraper->setText('  <p id="id_value">  Trimmed text</p>')->trimStripTags()->getText();
        $this->assertEquals('Trimmed text', $text);

        $texts = $this->scraper->setText([
            ' a <div class="div_class"><p> Inner value </p></div> bit at the end  ',
            ' zz<div id="div_id"><strong> Inner value </strong></div> odd bits  ',
        ])->trimStripTags()->getText();

        $this->assertSame([
            'a  Inner value  bit at the end',
            'zz Inner value  odd bits',
        ], $texts);
    }

    public function test_trim()
    {
        $text = $this->scraper->setText('  <p id="id_value">  Trimmed text</p> ')->trim()->getText();
        $this->assertEquals('<p id="id_value">  Trimmed text</p>', $text);

        $texts = $this->scraper->setText([
            ' a <div class="div_class"><p> Inner value </p></div> bit at the end  ',
            ' zz<div id="div_id"><strong> Inner value </strong></div> odd bits  ',
        ])->trim()->getText();

        $this->assertSame([
            'a <div class="div_class"><p> Inner value </p></div> bit at the end',
            'zz<div id="div_id"><strong> Inner value </strong></div> odd bits',
        ], $texts);
    }

    public function test_stripTags()
    {
        $text = $this->scraper->setText(' 1 <p id="id_value">  Trimmed text</p> abcd ')->stripTags()->getText();
        $this->assertEquals(' 1   Trimmed text abcd ', $text);

        $texts = $this->scraper->setText([
            ' a <div class="div_class"><p> Inner value </p></div> bit at the end  ',
            ' zz<div id="div_id"><strong> Inner value </strong></div> odd bits  ',
        ])->stripTags()->getText();

        $this->assertSame([
            ' a  Inner value  bit at the end  ',
            ' zz Inner value  odd bits  ',
        ], $texts);
    }

    public function test_firstOf()
    {
        $first = $this->scraper->setText('Only one value')->firstOf()->getText();
        $this->assertEquals('Only one value', $first);

        $first = $this->scraper->setText([
            'first',
            'second',
        ])->firstOf()->getText();
        $this->assertEquals('first', $first);

        $first = $this->scraper->setText([
            0,
            null,
            '',
            'first',
            'second',
        ])->firstOf()->getText();
        $this->assertEquals('first', $first);

        $first = $this->scraper->setText('')->firstOf()->getText();
        $this->assertEquals(false, $first);

        $first = $this->scraper->setText(0)->firstOf()->getText();
        $this->assertEquals(false, $first);

        $first = $this->scraper->setText(null)->firstOf()->getText();
        $this->assertEquals(false, $first);

        $first = $this->scraper->setText(false)->firstOf()->getText();
        $this->assertEquals(false, $first);

        $first = $this->scraper->setText([])->firstOf()->getText();
        $this->assertEquals(false, $first);
    }

    public function test_firstNonBlank()
    {
        $first = $this->scraper->setText('Only one value')->firstNonBlank()->getText();
        $this->assertEquals('Only one value', $first);

        $first = $this->scraper->setText([
            '',
            'first',
            'second',
        ])->firstNonBlank()->getText();
        $this->assertEquals('first', $first);

        $first = $this->scraper->setText([
            null,
            0,
            '',
            'first',
            'second',
        ])->firstNonBlank()->getText();
        $this->assertEquals(0, $first);

        $first = $this->scraper->setText('')->firstNonBlank()->getText();
        $this->assertEquals(false, $first);

        $first = $this->scraper->setText(0)->firstNonBlank()->getText();
        $this->assertEquals(0, $first);

        $first = $this->scraper->setText(null)->firstNonBlank()->getText();
        $this->assertEquals(false, $first);

        $first = $this->scraper->setText(false)->firstNonBlank()->getText();
        $this->assertEquals(false, $first);
    }

    public function test_first()
    {
        $first = $this->scraper->setText('Only one value')->first()->getText();
        $this->assertEquals('Only one value', $first);

        $first = $this->scraper->setText([
            'first',
            'second',
        ])->first()->getText();
        $this->assertEquals('first', $first);

        $first = $this->scraper->setText([
            0,
            null,
            '',
            'first',
            'second',
        ])->first()->getText();
        $this->assertEquals('0', $first);

        $first = $this->scraper->setText('')->first()->getText();
        $this->assertEquals('', $first);

        $first = $this->scraper->setText(0)->first()->getText();
        $this->assertEquals(0, $first);

        $first = $this->scraper->setText(null)->first()->getText();
        $this->assertEquals(null, $first);

        $first = $this->scraper->setText(false)->first()->getText();
        $this->assertEquals(false, $first);

        $first = $this->scraper->setText([])->first()->getText();
        $this->assertEquals(false, $first);
    }

    public function test_last()
    {
        $last = $this->scraper->setText('Only one value')->last()->getText();
        $this->assertEquals('Only one value', $last);

        $last = $this->scraper->setText([
            'first',
            'second',
        ])->last()->getText();
        $this->assertEquals('second', $last);

        $last = $this->scraper->setText([
            0,
            null,
            'first',
            'second',
            '',
        ])->last()->getText();
        $this->assertEquals('', $last);

        $last = $this->scraper->setText('')->last()->getText();
        $this->assertEquals('', $last);

        $last = $this->scraper->setText(0)->last()->getText();
        $this->assertEquals(0, $last);

        $last = $this->scraper->setText(null)->last()->getText();
        $this->assertEquals(null, $last);

        $last = $this->scraper->setText(false)->last()->getText();
        $this->assertEquals(false, $last);

        $last = $this->scraper->setText([])->last()->getText();
        $this->assertEquals(false, $last);
    }

    public function test_firstNumeric()
    {
        $first = $this->scraper->setText(10)->firstNumeric()->getText();
        $this->assertEquals(10, $first);

        $first = $this->scraper->setText([ 1, 2 ])->firstNumeric()->getText();
        $this->assertEquals(1, $first);

        $first = $this->scraper->setText([ 0, 1, 2 ])->firstNumeric()->getText();
        $this->assertEquals(0, $first);

        $first = $this->scraper->setText([ 1.1, 3, 4 ])->firstNumeric()->getText();
        $this->assertEquals(1.1, $first);

        $first = $this->scraper->setText([ -2, 13, 44 ])->firstNumeric()->getText();
        $this->assertEquals(-2, $first);

        $first = $this->scraper->setText([ 'a', -2, 13, 44 ])->firstNumeric()->getText();
        $this->assertEquals(-2, $first);

        $first = $this->scraper->setText([ null, -3, 13, 44 ])->firstNumeric()->getText();
        $this->assertEquals(-3, $first);

        $first = $this->scraper->setText([ '', 13, 44 ])->firstNumeric()->getText();
        $this->assertEquals(13, $first);

        $first = $this->scraper->setText([ '', 'a', 'b' ])->firstNumeric()->getText();
        $this->assertEquals(false, $first);
    }

    public function test_getValue()
    {
        $value = $this->scraper->setText($this->html)->getValue('input-name')->getText();
        $this->assertEquals('12340', $value);

        $value = $this->scraper->setText($this->html)->getValue('input-name-again')->getText();
        $this->assertEquals('5678', $value);

        $value = $this->scraper->setText($this->html)->getValue('input-name-which-does-not-exist')->getText();
        $this->assertEquals('', $value);

        $value = $this->scraper->setText($this->html)->getValue('zero-value')->getText();
        $this->assertSame('0', $value);
    }

    public function test_getDiv()
    {
        $div = $this->scraper->setText($this->html)->getDiv('')->getText();
        $this->assertEquals('', $div);

        $div = $this->scraper->setText($this->html)->getDiv('this-does-not-exist')->getText();
        $this->assertEquals('', $div);

        $div = $this->scraper->setText($this->html)->getDiv('id="description"')->getText();
        $this->assertEquals('This is a bit of text', $div);

        $div = $this->scraper->setText($this->html)->getDiv('<h1', 0, false)->getText();
        $this->assertEquals('This is a test file', $div);

        $div = $this->scraper->setText($this->html)->getDiv('<img', 0, false)->getText();
        $this->assertEquals('', $div);

        $div = $this->scraper->setText($this->html)->getDiv('<img', 0, true)->getText();
        $this->assertEquals('<img src="image.jpg" />', $div);

        $div = $this->scraper->setText($this->html)->getDiv('some-class')->getText();
        $this->assertEquals('Some class text', $div);

        $div = $this->scraper->setText($this->html)->getDiv('some-class', 1260)->getText();
        $this->assertEquals('Some class again', $div);

        $div = $this->scraper->setText($this->html)->getDiv('some-class', 0, true)->getText();
        $this->assertEquals('<div class="some-class">Some class text</div>', $div);

        $div = $this->scraper->setText($this->html)->getDiv('some-class', 1260, true)->getText();
        $this->assertEquals('<div class="some-class">Some class again</div>', $div);

        $div = $this->scraper->setText($this->html)->getDiv('This file is use ')->getText();
        $this->assertEquals('This file is use to run tests on Crawler', $div);

        $div = $this->scraper->setText($this->html)->getDiv([
            '<title',
            'some-class',
        ])->getText();

        $this->assertSame([
            'Test File',
            'Some class text',
        ], $div);
    }

    public function test_getDivRepeat()
    {
        $div = $this->scraper->setText($this->html)->getDivRepeat('')->getText();
        $this->assertSame([], $div);

        $div = $this->scraper->setText($this->html)->getDivRepeat('this-does-not-exist')->getText();
        $this->assertSame([], $div);

        $div = $this->scraper->setText($this->html)->getDivRepeat('id="description"')->getText();
        $this->assertSame([ 'This is a bit of text' ], $div);

        $div = $this->scraper->setText($this->html)->getDivRepeat('<h1', 0, false)->getText();
        $this->assertSame([ 'This is a test file' ], $div);

        $div = $this->scraper->setText($this->html)->getDivRepeat('some-class')->getText();
        $this->assertSame([
            'Some class text',
            'Some class again'
        ], $div);

        $div = $this->scraper->setText($this->html)->getDivRepeat('some-class', 0, true)->getText();
        $this->assertSame([
            '<div class="some-class">Some class text</div>',
            '<div class="some-class">Some class again</div>'
        ], $div);

        $div = $this->scraper->setText($this->html)->getDivRepeat('some-class', 1260)->getText();
        $this->assertSame([ 'Some class again' ], $div);

        $div = $this->scraper->setText($this->html)->getDivRepeat('some-class', 1260, true)->getText();
        $this->assertSame([ '<div class="some-class">Some class again</div>' ], $div);

        $div = $this->scraper->setText($this->html)->getDivRepeat('<li>', 1260)->getText();
        $this->assertSame([
            'list 1',
            'list 2',
            'list 3',
            'list 4',
        ], $div);

        $div = $this->scraper->setText($this->html)->getDivRepeat('zero-value', 0, true)->getText();
        $this->assertSame([
            "<input name='zero-value' value='0'>",
            "<input name='zero-value-1' value='1'>",
        ], $div);
    }

    public function test_removeInlineCss()
    {
        $text = $this->scraper
                     ->setText('Some text <style>h1 { font-size: 20px; }</style> more text ')
                     ->removeInlineCss()
                     ->getText();

        $this->assertEquals('Some text  more text ', $text);

        $text = $this->scraper
                     ->setText('Some text without inline style ')
                     ->removeInlineCss()
                     ->getText();

        $this->assertEquals('Some text without inline style ', $text);
    }

    public function test_removeInlineJavascript()
    {
        $text = $this->scraper
                     ->setText('Some text <script>var abc=1;</script> more text ')
                     ->removeInlineJavascript()
                     ->getText();

        $this->assertEquals('Some text  more text ', $text);

        $text = $this->scraper
                     ->setText('Some text without inline js')
                     ->removeInlineJavascript()
                     ->getText();

        $this->assertEquals('Some text without inline js', $text);
    }

    public function test_removeHtmlComments()
    {
        $text = $this->scraper
                     ->setText('Some text <!-- HTML comments --> more text')
                     ->removeHtmlComments()
                     ->getText();

        $this->assertEquals('Some text  more text', $text);

        $text = $this->scraper
                     ->setText('Some text without comments')
                     ->removeHtmlComments()
                     ->getText();

        $this->assertEquals('Some text without comments', $text);
    }

    public function test_urlDeocde()
    {
        $text = $this->scraper->setText(urlencode('my=apples&are=green+and+red'))->urlDecode()->getText();
        $this->assertEquals('my=apples&are=green+and+red', $text);

        $text = $this->scraper->setText([
            urlencode('my=apples&are=green+and+red'),
            urlencode('my=oranges&are=orange'),
        ])->urlDecode()->getText();

        $this->assertSame([ 'my=apples&are=green+and+red', 'my=oranges&are=orange' ], $text);
    }

    public function test_arrayMerge()
    {
        $array = $this->scraper
                      ->setText([ 'first', 'second'])
                      ->arrayMerge('third', 'fourth')
                      ->getText();

        $this->assertSame([ 'first', 'second', 'third', 'fourth' ], $array);

        $array = $this->scraper
                      ->setText([ 'first', 'second'])
                      ->arrayMerge([ 'third', 'fourth' ])
                      ->getText();

        $this->assertSame([ 'first', 'second', 'third', 'fourth' ], $array);

        $array = $this->scraper
                      ->setText([ 'first', 'second'])
                      ->arrayMerge([])
                      ->getText();

        $this->assertSame([ 'first', 'second' ], $array);

        $array = $this->scraper
                      ->setText([ 'first', 'second'])
                      ->arrayMerge(1, 2)
                      ->getText();

        $this->assertSame([ 'first', 'second', 1, 2 ], $array);

        $array = $this->scraper
                      ->setText([ 'first', 'second'])
                      ->arrayMerge(null)
                      ->getText();

        $this->assertSame([ 'first', 'second' ], $array);

        $array = $this->scraper
                      ->setText([ 'first', 'second'])
                      ->arrayMerge('')
                      ->getText();

        $this->assertSame([ 'first', 'second', '' ], $array);


        $array = $this->scraper
                      ->setText([ 'first', 'second'])
                      ->arrayMerge([ 1, 2 ], [ 3, 4 ])
                      ->getText();

        $this->assertSame([ 'first', 'second', 1, 2, 3, 4 ], $array);
    }

    public function test_shorterOf()
    {
        $text = $this->scraper->setText([ 'abc', 'abcd', 'abcde' ])->shorterOf()->getText();
        $this->assertEquals('abc', $text);

        $text = $this->scraper->setText([ 'abcdef', 'abcd', 'abcde' ])->shorterOf()->getText();
        $this->assertEquals('abcd', $text);

        $text = $this->scraper->setText([ 'abcdef', 'abcd', 'abcde', '' ])->shorterOf()->getText();
        $this->assertEquals('', $text);

        $text = $this->scraper->setText([ 'abcdef', 'abcd', 'abcde', 0 ])->shorterOf()->getText();
        $this->assertEquals(0, $text);

        $text = $this->scraper->setText([ 'efgh', 'abcd', 'wxyz' ])->shorterOf()->getText();
        $this->assertEquals('abcd', $text);

        $text = $this->scraper->setText([ 'efgh', 'abcd', 'wxyz', false ])->shorterOf()->getText();
        $this->assertEquals(false, $text);

        $text = $this->scraper->setText([ 'efgh', 'abcd', 'wxyz', null ])->shorterOf()->getText();
        $this->assertEquals(null, $text);

        $text = $this->scraper->setText([ 100, 3568, 25 ])->shorterOf()->getText();
        $this->assertEquals(25, $text);

        $text = $this->scraper->setText([ 300, 3568, 259, -1786 ])->shorterOf()->getText();
        $this->assertEquals(259, $text);
    }

    public function test_shortest()
    {
        $text = $this->scraper->setText([ 'abc', 'abcd', 'abcde' ])->shortest()->getText();
        $this->assertEquals('abc', $text);

        $text = $this->scraper->setText([ 'abcdef', 'abcd', 'abcde' ])->shortest()->getText();
        $this->assertEquals('abcd', $text);

        $text = $this->scraper->setText([ 'abcdef', 'abcd', 'abcde', '' ])->shortest()->getText();
        $this->assertEquals('', $text);

        $text = $this->scraper->setText([ 'abcdef', 'abcd', 'abcde', 0 ])->shortest()->getText();
        $this->assertEquals(0, $text);

        $text = $this->scraper->setText([ 'efgh', 'abcd', 'wxyz' ])->shortest()->getText();
        $this->assertEquals('abcd', $text);

        $text = $this->scraper->setText([ 'efgh', 'abcd', 'wxyz', false ])->shortest()->getText();
        $this->assertEquals(false, $text);

        $text = $this->scraper->setText([ 'efgh', 'abcd', 'wxyz', null ])->shortest()->getText();
        $this->assertEquals(null, $text);

        $text = $this->scraper->setText([ 100, 3568, 25 ])->shortest()->getText();
        $this->assertEquals(25, $text);

        $text = $this->scraper->setText([ 300, 3568, 259, -1786 ])->shortest()->getText();
        $this->assertEquals(259, $text);
    }

    public function test_removeBlankStrings()
    {
        $texts = $this->scraper->setText('string')->removeBlankStrings()->getText();
        $this->assertEquals('string', $texts);

        $texts = $this->scraper->setText([
            'string',
            '',
            null,
            false,
            0,
            123,
        ])->removeBlankStrings()->getText();

        $this->assertSame([
            'string',
            0,
            123,
        ], $texts);
    }

    public function test_append()
    {
        $text = $this->scraper->setText('-text-')->append('suffix')->getText();
        $this->assertSame('-text-suffix', $text);

        $text = $this->scraper->setText([
            '-first-',
            '-second-',
            '-third-',
        ])->append('suffix')->getText();

        $this->assertSame([
            '-first-suffix',
            '-second-suffix',
            '-third-suffix',
        ], $text);
    }

    public function test_prepend()
    {
        $text = $this->scraper->setText('-text-')->prepend('prefix')->getText();
        $this->assertSame('prefix-text-', $text);

        $text = $this->scraper->setText([
            '-first-',
            '-second-',
            '-third-',
        ])->prepend('prefix')->getText();

        $this->assertSame([
            'prefix-first-',
            'prefix-second-',
            'prefix-third-',
        ], $text);
    }

    public function test_compressHtml()
    {
        $html = $this->scraper
                     ->setText(" \n start\n<div class='some-class'>  stuff <br>  </div>\n\n  end ")
                     ->compressHtml()
                     ->getText();

        $this->assertSame("start<div class='some-class'>stuff<br></div>end", $html);

        $html = $this->scraper
                     ->setText([
                        " \n start\n<div class='some-class'>  stuff <br>  </div>\n\n  end ",
                        "<p><div class='some-class'>  stuff 111<br>  </div>\n\n",
                     ])
                     ->compressHtml()
                     ->getText();

        $this->assertSame([
            "start<div class='some-class'>stuff<br></div>end",
            "<p><div class='some-class'>stuff 111<br></div>",
        ], $html);
    }

    public function test_getPostFields()
    {
        $postfields = $this->scraper->setText($this->html)->getPostFields(true)->getText();
        $this->assertSame([
            'input-name' => '12340',
            'input-name-again' => '5678',
            'zero-value' => '0',
            'zero-value-1' => '1',
            'textbox-value' => 'some-value',
            'checkbox-checked' => 1,
        ], $postfields);

        $postfields = $this->scraper->setText($this->html)->getPostFields(false)->getText();
        $this->assertSame([
            'input-name' => '12340',
            'input-name-again' => '5678',
            'zero-value' => '0',
            'zero-value-1' => '1',
            'textbox-value' => 'some-value',
        ], $postfields);
    }

    public function test_asNumeric()
    {
        $this->assertEquals(11, $this->scraper->setText(11)->asNumeric());
        $this->assertEquals(-22, $this->scraper->setText(-22)->asNumeric());
        $this->assertEquals(0.3, $this->scraper->setText(0.3)->asNumeric());
        $this->assertEquals(0, $this->scraper->setText(0)->asNumeric());
        $this->assertEquals(99, $this->scraper->setText('99')->asNumeric());

        $this->assertEquals(0, $this->scraper->setText('abc')->asNumeric());
        $this->assertEquals(0, $this->scraper->setText([ 1, 2, 3 ])->asNumeric());
    }

    public function test_explode()
    {
        $this->assertEquals(['ab', 'cd'], $this->scraper->setText('ab,cd')->explode(',')->getText());
    }
}
