<?php

namespace Scraper;

class ScraperHelpers
{
    /**
    * Applies a callback function to a string or values of an array
    *
    * @param  string|array  $callback The callback function
    * @param  string|array  $text     Array or string to apply the callback to
    * @return string|array
    */
    private static function each($callback, $text, ...$args)
    {
        $transformed = array_map(function ($value) use ($callback, $args) {
            return call_user_func_array($callback, array_merge((array) $value, $args));
        }, (array) $text);

        return (gettype($text) == 'array') ? $transformed : $transformed[0];
    }

    /**
    * Checks if a variable is a multidimensional array
    * @param  $array
    * @return bool
    **/
    private static function isMultidimensionalArray($array)
    {
        if (!is_array($array)) {
            return false;
        }

        return count($array) != count($array, COUNT_RECURSIVE);
    }

    /**
    * Finds a pattern in a string and returns the string between the 2nd last and last match
    * An array of patterns can also be passed (returns an array of strings)
    *
    * @param  array        $pattern  Array of strings to match in sequence
    * @param  string       $text     The haystack to search in
    * @param  int          $start    The starting position
    * @return array|string           The string between 2nd last and last match in sequence. Returns '' if no match.
    */
    public static function getPattern(array $pattern, string $text, $start = 0)
    {
        if (self::isMultidimensionalArray($pattern)) {
            return array_map(function ($p) use ($text, $start) {
                return self::getPatternFromString($p, $text, $start);
            }, $pattern);
        }

        return self::getPatternFromString($pattern, $text, $start);
    }

    /**
    * Finds a pattern in a string and returns the string between the 2nd last and last match
    *
    * @param  string $text     The haystack to search in
    * @param  array  $pattern  Array of strings to match in sequence
    * @param  int    $start    The starting position
    * @return string           The string between 2nd last and last match in sequence. Returns '' if no match.
    */
    public static function getPatternFromString(array $pattern, string $text, $start = 0)
    {
        $matches = self::getPatternRepeat($pattern, $text, $start);
        return isset($matches[0]) ? $matches[0] : '';
    }

    /**
    * Repeatedly finds a pattern in a string and returns the string between the 2nd last and last match; as an array of strings
    *
    * @param  array  $pattern  Array of strings to match in sequence
    * @param  string $text     The haystack to search in
    * @param  int    $start    The starting position (default 0)
    * @return array            Array of strings matched
    */
    public static function getPatternRepeat(array $pattern, string $text, $start = 0)
    {
        if (self::isMultidimensionalArray($pattern)) {
            return array_map(function ($p) use ($text, $start) {
                return self::getPatternRepeatFromString($p, $text, $start);
            }, $pattern);
        }

        return self::getPatternRepeatFromString($pattern, $text, $start);
    }

    /**
    * Repeatedly finds a pattern in a string and returns the string between the 2nd last and last match; as an array of strings
    *
    * @param  array  $pattern  Array of strings to match in sequence
    * @param  string $text     The haystack to search in
    * @param  int    $start    The starting position (default 0)
    * @return array            Array of strings matched
    */
    public static function getPatternRepeatFromString(array $pattern, string $text, $start = 0)
    {
        if ($start === false || $pattern == []) {
            return [];
        }

        $start = is_numeric($start) ? $start : 0;

        if (!preg_match_all(self::getPatternRegex($pattern), $text, $matches, 0, $start)) {
            return [];
        }

        return array_map(function ($match) {
            return trim($match);
        }, $matches[1]);
    }

    /**
    * Returns the regex required in getPattern functions
    *
    * @param  array  $pattern  Array of strings to match in sequence
    * @return string
    */
    private static function getPatternRegex($pattern)
    {
        $pattern = array_map(function ($p) {
            return preg_quote($p, '/');
        }, $pattern);

        $last = array_pop($pattern);

        return '/'.implode('[\s\S]*?', $pattern).'([\s\S]*?)'.$last.'/';
    }

    /**
    * Removes all sections of a string (or array of strings) between two parts, inclusive of $from and $to
    *
    * @param  string        $from  Starting string
    * @param  string        $to    Ending string
    * @param  string|array  $text  The haystack
    * @return string|array         The string or array with section removed
    */
    public static function removeBetween(string $from, string $to, $text)
    {
        return self::each(
            [ 'Scraper\ScraperHelpers', 'removeBetweenFromString' ],
            $text, $from, $to
        );
    }

    /**
    * Removes all sections of a string between two parts, inclusive of $from and $to
    *
    * @param  string $text  The haystack
    * @param  string $from  Starting string
    * @param  string $to    Ending string
    * @return string        The string or array with section removed
    */
    private static function removeBetweenFromString($text, string $from, string $to)
    {
        return preg_replace(self::removeBetweenRegex($from, $to), '', $text);
    }

    /**
    * Returns the regex pattern used in removeBetween functions
    *
    * @param  string $from  Starting string
    * @param  string $to    Ending string
    * @return string
    **/
    private static function removeBetweenRegex($from, $to)
    {
        return '/'.preg_quote($from, '/').'[\s\S]*?'.preg_quote($to, '/').'/';
    }

    /**
    * Finds the position of the first number in a string
    *
    * @param  string $text  The string to search
    * @return int           The position of the first number, or false if no number in string
    */
    public static function getFirstNumberPosition(string $text, $start = 0)
    {
        $text = ($start > 0) ? $text = substr($text, $start) : $text;

        if (preg_match('/\d/', $text, $match, PREG_OFFSET_CAPTURE)) {
            return $match[0][1] + $start;
        }

        return false;
    }

    /**
    * Removes non-numeric characters from a string or array of strings
    *
    * @param  string|array $text               String or array to remove non-numeric characters from
    * @param  bool         $keepCommaDecimals  TRUE to keep decimal point and commas
    * @return string|array
    */
    public static function removeNonNumericCharacters($text, $keepDecimalPoint = false)
    {
        return self::each(
            ['Scraper\ScraperHelpers', 'removeNonNumericCharactersFromString'],
            $text, $keepDecimalPoint
        );
    }

    /**
    * Removes non-numeric characters from a string
    *
    * @param  string $text               String to remove non-numeric characters from
    * @param  bool   $keepCommaDecimals  TRUE to keep decimal point and commas
    * @return string
    */
    private static function removeNonNumericCharactersFromString(string $text, $keepDecimalPoint = false)
    {
        return $keepDecimalPoint ? preg_replace("/[^0-9\.]/", '', $text) : preg_replace("/[^0-9]/", '', $text);
    }

    /**
    * trim from string or array of strings
    *
    * @param  string|array  $text
    * @return string|array
    */
    public static function trim($text)
    {
        return self::each('trim', $text);
    }

    /**
    * strip_tags from string or array of strings
    *
    * @param  string|array  $text
    * @return string|array
    */
    public static function stripTags($text)
    {
        return self::each('strip_tags', $text);
    }

    /**
    * trim and strip_tags from string or array of strings
    *
    * @param  string|array  $text
    * @return string|array
    */
    public static function trimStripTags($text)
    {
        return self::trim( self::stripTags($text) );
    }

    /**
    * Applies urldecode to $text
    *
    * @param  array|string  $text
    * @return array|string
    */
    public static function urlDecode($text)
    {
        return self::each('urldecode', $text);
    }

    /**
    * Returns first non empty, non-zero, non-false value of an array, or return false if all empty
    *
    * @param  array $values
    * @return
    */
    public static function firstOf($values)
    {
        foreach ((array) $values as $value) {
            if ($value) {
                return $value;
            }
        }

        return false;
    }

    /**
    * Returns first non blank value of an array, of false if all blank
    *
    * @param  array $values
    * @return
    */
    public static function firstNonBlank($values)
    {
        foreach ((array) $values as $value) {
            if ((string) $value != '') {
                return $value;
            }
        }

        return false;
    }

    /**
    * Returns first numeric value of an array, or false if all non-numeric
    *
    * @param  array $values
    * @return
    */
    public static function firstNumeric($values)
    {
        foreach ((array) $values as $value) {
            if (is_numeric($value)) {
                return $value;
            }
        }

        return false;
    }

    /**
    * Gets value from <input name='$name' value='...'> tags
    *
    * @param  string $name  The name attribute
    * @param  string $html  The HTML of page
    * @return string        The value
    */
    public static function getValue(string $name, string $html)
    {
        return self::firstNonBlank([
            self::getPattern([ 'name="'.$name.'"', 'value="', '"' ], $html),
            self::getPattern([ "name='".$name."'", "value='", "'" ], $html),
        ]);
    }

    /**
    * Returns postfields within $html as array in format 'name' => 'value'
    *
    * @param  string $html  The HTML of page
    * @return array         The postfield name/value pairs
    */
    public static function getPostFields(string $html, $includeCheckboxes = false)
    {
        $inputs = self::getDivRepeat(' name=', $html, 0, true);
        $postfields = [];

        foreach ($inputs as $input) {
            $type = strtolower(self::firstNonBlank([
                self::getPattern([ 'type="', '"' ], $input),
                self::getPattern([ "type='", "'" ], $input),
            ]));

            $name = self::firstNonBlank([
                self::getPattern([ 'name="', '"' ], $input),
                self::getPattern([ "name='", "'" ], $input),
            ]);

            $value = self::firstNonBlank([
                self::getPattern([ 'value="', '"' ], $input),
                self::getPattern([ "value='", "'" ], $input),
            ]);

            if ($type == 'checkbox') {
                if (strpos(strtolower(str_replace([ $name, $value ], '', $input)), 'checked') !== false) {
                    $value = 1;
                }
                else {
                    $name = '';
                }
            }

            if ($name && $type != 'submit') {
                if ($type != 'checkbox' || $includeCheckboxes) {
                    $postfields[$name] = $value;
                }
            }
        }

        return $postfields;
    }

    /**
    * Finds the string between a <div></div> or other <*></*> tag
    * e.g. <div $seed> ...</div>
    *
    * @param  string|array $seed       The string to find within $html. Will return the string between tags that contains this string.
    * @param  string       $html       The haystack to search in
    * @param  int          $start      The starting position (default 0)
    * @param  bool         $inclusive  TRUE returns surrounding tags. FALSE otherwise
    * @return string|array             The string (or array of strings) within the <*></*> tags
    */
    public static function getDiv($seed, string $html, $start = 0, $inclusive = false)
    {
        return self::each(
            [ 'Scraper\ScraperHelpers', 'getDivFromString' ],
            $seed, $html, $start, $inclusive
        );
    }

    /**
    * Finds the string between a <div></div> or other <*></*> tag
    * e.g. <div $seed> ...</div>
    *
    * @param  string $seed       The string to find within $html. Will return the string between tags that contains this string.
    * @param  string $html       The haystack to search in
    * @param  int    $start      The starting position (default 0)
    * @param  bool   $inclusive  TRUE returns surrounding tags. FALSE otherwise
    * @return string             The string within the <*></*> tags
    */
    private static function getDivFromString(string $seed, string $html, $start = 0, $inclusive = false)
    {
        $data = self::getDivFull($seed, $html, $start, $inclusive);
        return ($data === '') ? '' : $data['data'];
    }

    /**
    * Finds the string between a <div></div> or other <*></*> tag
    * i.e. <div $seed> ...</div>
    * Also returns the position of the last match
    *
    * @param  string $seed       The string to find within $html. Will return the string between tags that contains this string.
    * @param  string $html       The haystack to search in
    * @param  int    $start      The starting position (default 0)
    * @param  bool   $inclusive  TRUE returns surrounding tags
    * @return array
    *       - string 'data'      The string within the <*></*> tags
    *       - int    'end'       The position of the end of the closing tag </*>
    */
    public static function getDivFull($seed, string $html, $start = 0, $inclusive = false)
    {
        if ($seed == '') {
            return '';
        }

        $start = $start > strlen($html) ? strlen($html) : $start;
        $start = strpos($html, $seed, $start);

        // Seed not found
        if ($start === false) {
            return '';
        }

        $firstOpenPosition = self::previousOpenPosition($html, $start + 1);
        $firstClosePosition = self::previousClosePosition($html, $start + 1);

        // $seed is between tags i.e. <*>$seed</*>
        if ($firstClosePosition >= $firstOpenPosition) {
            $nextOpenPosition = self::nextOpenPosition($html, $firstClosePosition + 1);
            return [
                'data' => substr($html, $firstClosePosition + 1, $nextOpenPosition - $firstClosePosition - 1),
                'end' => $firstClosePosition + 1,
            ];
        }

        $start = $firstOpenPosition;
        $tagType = self::getTagType(substr($html, $start));

        // Singluar tags. e.g. <img> doesn't need closing tag
        if (self::isSingluarTag($tagType)) {
            if (!$inclusive) {
                return '';
            }

            $nextClosePosition = self::nextClosePosition($html, $start);

            return [
                'data' => substr($html, $start, $nextClosePosition - $start + 1),
                'end' => $nextClosePosition + 1,
            ];
        }

        $end = $start + strlen($tagType);
        $count = 1;
        $tagTypeClose = '</'.trim(str_replace('<', '', $tagType));
        $endOfString = false;

        do
        {
            $nextOpenPosition = self::nextPosition($tagType, $html, $end);
            $nextClosePosition = self::nextPosition($tagTypeClose, $html, $end);

            if ($nextOpenPosition > $nextClosePosition) {
                $count--;
                $end = $nextClosePosition + strlen($tagTypeClose);
            }

            if ($nextOpenPosition < $nextClosePosition) {
                $count++;
                $end = $nextOpenPosition + strlen($tagType);
            }

            if ($nextOpenPosition == $nextClosePosition) {
                $count = 0;
                $end = $nextOpenPosition;
                $endOfString = true;
            }
        } while ($count > 0);

        if (!$inclusive) {
            $start = self::nextClosePosition($html, $start) + 1;
            if (!$endOfString) {
                $end = self::previousOpenPosition($html, $end) - 1;
            }
        }
        else {
            $end = self::nextClosePosition($html, $end);
        }

        return [
            'data' => trim(substr($html, $start, $end - $start + 1)),
            'end' => $end,
        ];
    }

    /**
    * Returns TRUE if a html tag does not require closing tags
    *
    * @param  string $tagType
    * @return bool
    **/
    private static function isSingluarTag($tagType)
    {
        $tagType = self::firstOf([
            self::getTagType($tagType),
            $tagType,
        ]);

        // Closing tags are considered singular
        if (substr($tagType, 0, 2) == '</') {
            return true;
        }

        $tagType = str_replace('</', '', $tagType);
        $tagType = str_replace('<', '', $tagType);
        $tagType = strtolower(trim($tagType));

        return in_array($tagType, [ 'img', 'br', 'hr', 'input', 'link', '!--' ]);
    }

    /**
    * Returns next position of the < character in a string
    * Returns the strlen if not found
    *
    * @param  string $haystack  String to search in
    * @param  int    $start     Starting position
    * @return int
    **/
    private static function nextOpenPosition($haystack, $start)
    {
        return self::nextPosition('<', $haystack, $start);
    }

    /**
    * Returns next position of the > character in a string
    * Returns the strlen if not found
    *
    * @param  string $haystack  String to search in
    * @param  int    $start     Starting position
    * @return int
    **/
    private static function nextClosePosition($haystack, $start)
    {
        return self::nextPosition('>', $haystack, $start);
    }

    /**
    * Returns next position of string within a haystack
    * Returns the strlen if not found
    *
    * @param  string $needle    String to search for
    * @param  string $haystack  String to search in
    * @param  int    $start     Starting position
    * @return int
    **/
    public static function nextPosition($needle, $haystack, $start = 0)
    {
        if ($start > strlen($haystack)) {
            return strlen($haystack);
        }

        $start = ($start < 0) ? 0 : $start;

        $position = strpos($haystack, $needle, $start);
        return $position !== false ? $position: strlen($haystack);
    }

    /**
    * Returns previous position of the > character in a string
    * Returns -1 if not found
    *
    * @param  string $haystack  String to search in
    * @param  int    $start     Starting position
    * @return int
    **/
    private static function previousClosePosition($haystack, $start)
    {
        return self::previousPosition('>', $haystack, $start);
    }

    /**
    * Returns previous position of the < character in a string
    * Returns -1 if not found
    *
    * @param  string $haystack  String to search in
    * @param  int    $start     Starting position
    * @return int
    **/
    private static function previousOpenPosition($haystack, $start)
    {
        return self::previousPosition('<', $haystack, $start);
    }

    /**
    * Returns previous position of string within a haystack
    * Returns -1 if not found
    *
    * @param  string $needle    String to search for
    * @param  string $haystack  String to search in
    * @param  int    $start     Starting position
    * @return int
    **/
    public static function previousPosition($needle, $haystack, $start = null)
    {
        if ($start < 0) {
            return -1;
        }

        if ($start === null) {
            $start = strlen($haystack);
        }

        $start = ($start > strlen($haystack)) ? strlen($haystack) : $start;

        $position = strrpos($haystack, $needle, -strlen($haystack) + $start);
        return $position !== false ? $position : -1;
    }

    /**
    * Returns start of html tag. e.g. <input
    *
    * @param  string $tag
    * @return int
    **/
    private static function getTagType($tag)
    {
        if (strlen($tag) > 1000) {
            $tag = substr($tag, 0, 1000);
        }

        return self::shorterOf(
            self::removeEmpty([
                self::getPattern([ ' ' ], $tag),
                self::getPattern([ '>' ], $tag),
            ])
        );
    }

    /**
    * Finds the string between a <div></div> or other <*></*> tag, repeatedly until end of $html
    * i.e. <*>... $seed ...</*>
    *
    * @param  string $html       The haystack to search in
    * @param  string $seed       The string to find within $html. Will return the string between tags that contains this string.
    * @param  int    $start      The starting position (default 0)
    * @param  bool   $inclusive  TRUE returns surrounding tags. FALSE otherwise
    * @param  int    $loopMax    Maximum number of loops. Needed to prevent infinite loops.
    * @return array
    *       - string 'data'      The string within the <*></*> tags
    *       - int    'end'       The position of the end of the closing tag </*>
    */
    public static function getDivRepeat(string $seed, string $html, $start = 0, $inclusive = false, $loopMax = 100)
    {
        if ($seed == '') {
            return [];
        }

        if (!is_numeric($start)) {
            $start = strpos($html, $start);

            if ($start === false) {
                return [];
            }
        }

        $final = [];

        do {
            $data = self::getDivFull($seed, $html, $start, $inclusive);
            if ($data != '') {
                $final[] = $data['data'];
                $start = $data['end'];
            }
        } while ($data != '' && $loopMax-- > 0);

        return $final;
    }

    /**
    * Removes all text between <style and /style>
    *
    * @param  string $html
    * @return string
    */
    public static function removeInlineCss($html)
    {
        return self::removeBetween('<style', '/style>', $html);
    }

    /**
    * Removes all text between <script and /script>
    *
    * @param  string $html
    * @return string
    */
    public static function removeInlineJavascript($html)
    {
        return self::removeBetween('<script', '/script>', $html);
    }

    /**
    * Removes all text between <!-- and -->
    *
    * @param  string $html
    * @return string
    */
    public static function removeHtmlComments($html)
    {
        return self::removeBetween('<!--', '-->', $html);
    }

    /**
    * Removes empty values from an array
    *
    * @param  array $array
    * @return array
    */
    public static function removeEmpty($array)
    {
        return is_array($array) ? array_values(array_filter($array)) : $array;
    }

    /**
    * Removes empty values from an array
    *
    * @param  array $array
    * @return array
    */
    public static function removeBlankStrings($array)
    {
        if (!is_array($array)) {
            return $array;
        }

        return array_values(array_filter($array, function ($part) {
            return (string) $part != '';
        }));
    }

    /**
    * Returns the shorter string in an array of strings
    *
    * @param  array|string  $texts
    * @return string
    **/
    public static function shorterOf($texts)
    {
        $texts = (array) $texts;

        usort($texts, function ($a, $b) {
            if (strlen((string) $a) != strlen((string) $b)) {
                return strlen((string) $a) - strlen((string) $b);
            }

            return (string) $a > (string) $b;
        });

        return reset($texts);
    }

    /**
    * Removes blank spaces and newlines between html tags
    *
    * @param  array|string  $html
    * @return array|string
    **/
    public static function compressHtml($html)
    {
        return self::each([ 'Scraper\ScraperHelpers', 'compressHtmlOfString' ], $html);
    }

    /**
    * Removes blank spaces and newlines between html tags
    *
    * @param  string  $html
    * @return string
    **/
    private static function compressHtmlOfString(string $html)
    {
        $parts = explode('<', $html);
        $parts = self::trim($parts);
        $first = array_shift($parts);
        $parts = self::removeBlankStrings($parts);
        $parts = self::prepend('<', $parts);

        $html = implode('', $parts);

        $parts = explode('>', $html);
        $parts = self::trim($parts);
        $last = array_pop($parts);
        $parts = self::removeBlankStrings($parts);
        $parts = self::append('>', $parts);

        if ($first != '') {
            $parts = array_merge([ $first ], $parts);
        }

        if ($last != '') {
            $parts = array_merge($parts, [ $last ]);
        }

        foreach ($parts as $key => $part) {
            if ($key > 0 && strlen($part) > 0 && substr($part, 0, 1) != '<') {
                $parts[$key - 1] .= $parts[$key];
                $parts[$key] = '';
            }
        }

        $parts = self::removeBlankStrings($parts);

        return implode('', $parts);
    }

    /**
    * Appends a string to another string, or array of strings
    *
    * @param  string       $suffix   String to append
    * @param  string|array $strings  String or array of strings
    * @return string|array
    **/
    public static function append(string $suffix, $strings)
    {
        return self::each(function ($string) use ($suffix) {
            return $string.$suffix;
        }, $strings);
    }

    /**
    * Prepends a string to another string, or array of strings
    *
    * @param  string       $prefix   String to prepend
    * @param  string|array $strings  String or array of strings
    * @return string|array
    **/
    public static function prepend($prefix, $strings)
    {
        return self::each(function ($string) use ($prefix) {
            return $prefix.$string;
        }, $strings);
    }

    /**
    * Returns $value if numeric, otherwise zero
    *
    * @param  mixed      $value
    * @return int|float
    **/
    public static function asNumeric($value)
    {
        if (is_string($value)) {
            $value = trim($value);
        }

        return is_numeric($value) ? $value : 0;
    }
}
