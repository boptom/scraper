<?php

use Scraper\ScraperHelpers;

class ScraperHelpersTest extends TestCase
{
    public function test_getPattern()
    {
        $text = ScraperHelpers::getPattern([
            [ '<html>', '<title>', '</title>' ],
            [ '<table', '<td>', '</td>' ],
        ], $this->html);

        $this->assertSame([
            'Test File',
            'Row1, Data1',
        ], $text);

        $text = ScraperHelpers::getPattern([], $this->html);
        $this->assertEquals('', $text);

        $text = ScraperHelpers::getPattern([ '<head>' ], $this->html);
        $this->assertEquals('<html>', $text);

        $text = ScraperHelpers::getPattern([ '<html>', '<title>', '</title>' ], $this->html);
        $this->assertEquals('Test File', $text);

        $text = ScraperHelpers::getPattern([ '<html>', '<title>', '</title>' ], $this->html, 100);
        $this->assertEquals('', $text);

        $text = ScraperHelpers::getPattern([ '<table', '<td>', '</td>' ], $this->html);
        $this->assertEquals('Row1, Data1', $text);

        $text = ScraperHelpers::getPattern([ '<td>', '</td>' ], $this->html, 600);
        $this->assertEquals('Row2, Data1', $text);

        $text = ScraperHelpers::getPattern([ '<html>', '<title>', '</title>' ], $this->html, 'non-numeric');
        $this->assertEquals('Test File', $text);
    }

    public function test_getPatternRepeat()
    {
        $values = ScraperHelpers::getPatternRepeat([ '<th>', '</th>' ], $this->html);
        $this->assertSame([
            'First',
            'Second',
            'Third',
        ], $values);

        $values = ScraperHelpers::getPatternRepeat([
            [ '<th>', '</th>' ],
            [ '<li>', '</li>' ],
        ], $this->html);

        $this->assertSame([
            [ 'First', 'Second', 'Third' ],
            [ 'list 1', 'list 2', 'list 3', 'list 4' ],
        ], $values);

        $values = ScraperHelpers::getPatternRepeat([ 'this does', 'not exist' ], $this->html);
        $this->assertSame([], $values);
    }

    public function test_getFirstNumberPosition()
    {
        $position = ScraperHelpers::getFirstNumberPosition('abcd1234');
        $this->assertEquals(4, $position);

        $position = ScraperHelpers::getFirstNumberPosition('a1bcd1234', 4);
        $this->assertEquals(5, $position);
    }

    public function test_removeBetween()
    {
        $text = ScraperHelpers::removeBetween('/23', '"ab', '1/234"abcd-zz0/23lkjh"ab098');
        $this->assertEquals('1cd-zz0098', $text);

        $text = ScraperHelpers::removeBetween('23', 'ab', [
            '1234abcd-zz023lkjhab098',
            'j23kladj*):"dsabkajkljab24h',
        ]);

        $this->assertSame([
            '1cd-zz0098',
            'jkajkljab24h',
        ], $text);
    }

    public function test_removeNonNumeric()
    {
        $text = ScraperHelpers::removeNonNumericCharacters('21jk0h213-.sdl*7# ');
        $this->assertEquals('2102137', $text);

        $text = ScraperHelpers::removeNonNumericCharacters('21jk0h213-.sdl*7# ', true);
        $this->assertEquals('210213.7', $text);

        $texts = ScraperHelpers::removeNonNumericCharacters([
            'j3hkfh3#F=8H+,D*',
            'Dasdj0kd 4 AD2a8.&j',
            '92fsG:17"39\80/08 9?',
        ]);

        $this->assertSame([
            '338',
            '0428',
            '92173980089',
        ], $texts);
    }

    public function test_trimStripTags()
    {
        $text = ScraperHelpers::trimStripTags('  <p id="id_value">  Trimmed text</p>');
        $this->assertEquals('Trimmed text', $text);

        $texts = ScraperHelpers::trimStripTags([
            ' a <div class="div_class"><p> Inner value </p></div> bit at the end  ',
            ' zz<div id="div_id"><strong> Inner value </strong></div> odd bits  ',
        ]);

        $this->assertSame([
            'a  Inner value  bit at the end',
            'zz Inner value  odd bits',
        ], $texts);
    }

    public function test_trim()
    {
        $text = ScraperHelpers::trim('  <p id="id_value">  Trimmed text</p> ');
        $this->assertEquals('<p id="id_value">  Trimmed text</p>', $text);

        $texts = ScraperHelpers::trim([
            ' a <div class="div_class"><p> Inner value </p></div> bit at the end  ',
            ' zz<div id="div_id"><strong> Inner value </strong></div> odd bits  ',
        ]);

        $this->assertSame([
            'a <div class="div_class"><p> Inner value </p></div> bit at the end',
            'zz<div id="div_id"><strong> Inner value </strong></div> odd bits',
        ], $texts);
    }

    public function test_stripTags()
    {
        $text = ScraperHelpers::stripTags(' 1 <p id="id_value">  Trimmed text</p> abcd ');
        $this->assertEquals(' 1   Trimmed text abcd ', $text);

        $texts = ScraperHelpers::stripTags([
            ' a <div class="div_class"><p> Inner value </p></div> bit at the end  ',
            ' zz<div id="div_id"><strong> Inner value </strong></div> odd bits  ',
        ]);

        $this->assertSame([
            ' a  Inner value  bit at the end  ',
            ' zz Inner value  odd bits  ',
        ], $texts);
    }

    public function test_firstOf()
    {
        $first = ScraperHelpers::firstOf('Only one value');
        $this->assertEquals('Only one value', $first);

        $first = ScraperHelpers::firstOf([
            'first',
            'second',
        ]);
        $this->assertEquals('first', $first);

        $first = ScraperHelpers::firstOf([
            0,
            null,
            '',
            'first',
            'second',
        ]);
        $this->assertEquals('first', $first);

        $first = ScraperHelpers::firstOf('');
        $this->assertEquals(false, $first);

        $first = ScraperHelpers::firstOf(0);
        $this->assertEquals(false, $first);

        $first = ScraperHelpers::firstOf(null);
        $this->assertEquals(false, $first);

        $first = ScraperHelpers::firstOf(false);
        $this->assertEquals(false, $first);
    }

    public function test_firstNonBlank()
    {
        $first = ScraperHelpers::firstNonBlank('Only one value');
        $this->assertEquals('Only one value', $first);

        $first = ScraperHelpers::firstNonBlank([
            '',
            'first',
            'second',
        ]);
        $this->assertEquals('first', $first);

        $first = ScraperHelpers::firstNonBlank([
            null,
            0,
            '',
            'first',
            'second',
        ]);
        $this->assertEquals(0, $first);

        $first = ScraperHelpers::firstNonBlank('');
        $this->assertEquals(false, $first);

        $first = ScraperHelpers::firstNonBlank(0);
        $this->assertEquals(0, $first);

        $first = ScraperHelpers::firstNonBlank(null);
        $this->assertEquals(false, $first);

        $first = ScraperHelpers::firstNonBlank(false);
        $this->assertEquals(false, $first);
    }

    public function test_firstNumeric()
    {
        $first = ScraperHelpers::firstNumeric(10);
        $this->assertEquals(10, $first);

        $first = ScraperHelpers::firstNumeric([ 1, 2 ]);
        $this->assertEquals(1, $first);

        $first = ScraperHelpers::firstNumeric([ 0, 1, 2 ]);
        $this->assertEquals(0, $first);

        $first = ScraperHelpers::firstNumeric([ 1.1, 3, 4 ]);
        $this->assertEquals(1.1, $first);

        $first = ScraperHelpers::firstNumeric([ -2, 13, 44 ]);
        $this->assertEquals(-2, $first);

        $first = ScraperHelpers::firstNumeric([ 'a', -2, 13, 44 ]);
        $this->assertEquals(-2, $first);

        $first = ScraperHelpers::firstNumeric([ null, -3, 13, 44 ]);
        $this->assertEquals(-3, $first);

        $first = ScraperHelpers::firstNumeric([ '', 13, 44 ]);
        $this->assertEquals(13, $first);

        $first = ScraperHelpers::firstNumeric([ '', 'a', 'b' ]);
        $this->assertEquals(false, $first);
    }

    public function test_getValue()
    {
        $value = ScraperHelpers::getValue('input-name', $this->html);
        $this->assertEquals('12340', $value);

        $value = ScraperHelpers::getValue('input-name-again', $this->html);
        $this->assertEquals('5678', $value);

        $value = ScraperHelpers::getValue('input-name-which-does-not-exist', $this->html);
        $this->assertEquals('', $value);

        $value = ScraperHelpers::getValue('zero-value', $this->html);
        $this->assertSame('0', $value);
    }

    public function test_getDiv()
    {
        $div = ScraperHelpers::getDiv('', $this->html);
        $this->assertEquals('', $div);

        $div = ScraperHelpers::getDiv('this-does-not-exist', $this->html);
        $this->assertEquals('', $div);

        $div = ScraperHelpers::getDiv('id="description"', $this->html);
        $this->assertEquals('This is a bit of text', $div);

        $div = ScraperHelpers::getDiv('<h1', $this->html, 0, false);
        $this->assertEquals('This is a test file', $div);

        $div = ScraperHelpers::getDiv('<img', $this->html, 0, false);
        $this->assertEquals('', $div);

        $div = ScraperHelpers::getDiv('<img', $this->html, 0, true);
        $this->assertEquals('<img src="image.jpg" />', $div);

        $div = ScraperHelpers::getDiv('some-class', $this->html);
        $this->assertEquals('Some class text', $div);

        $div = ScraperHelpers::getDiv('some-class', $this->html, 1260);
        $this->assertEquals('Some class again', $div);

        $div = ScraperHelpers::getDiv('some-class', $this->html, 0, true);
        $this->assertEquals('<div class="some-class">Some class text</div>', $div);

        $div = ScraperHelpers::getDiv('some-class', $this->html, 1260, true);
        $this->assertEquals('<div class="some-class">Some class again</div>', $div);

        $div = ScraperHelpers::getDiv('extra-div', $this->html);
        $this->assertEquals('<div><p>inner stuff</p></div>', $div);

        $div = ScraperHelpers::getDiv('extra-div', $this->html, 0, true);
        $this->assertEquals('<div class="extra-div"><div><p>inner stuff</p></div></div>', $div);

        $div = ScraperHelpers::getDiv('This file is use ', $this->html);
        $this->assertEquals('This file is use to run tests on Crawler', $div);

        $div = ScraperHelpers::getDiv([
            '<title',
            'some-class',
        ], $this->html);

        $this->assertSame([
            'Test File',
            'Some class text',
        ], $div);
    }

    public function test_getDivRepeat()
    {
        $div = ScraperHelpers::getDivRepeat('', $this->html);
        $this->assertSame([], $div);

        $div = ScraperHelpers::getDivRepeat('this-does-not-exist', $this->html);
        $this->assertSame([], $div);

        $div = ScraperHelpers::getDivRepeat('id="description"', $this->html);
        $this->assertSame([ 'This is a bit of text' ], $div);

        $div = ScraperHelpers::getDivRepeat('<h1', $this->html, 0, false);
        $this->assertSame([ 'This is a test file' ], $div);

        $div = ScraperHelpers::getDivRepeat('some-class', $this->html);
        $this->assertSame([
            'Some class text',
            'Some class again'
        ], $div);

        $div = ScraperHelpers::getDivRepeat('some-class', $this->html, 0, true);
        $this->assertSame([
            '<div class="some-class">Some class text</div>',
            '<div class="some-class">Some class again</div>'
        ], $div);

        $div = ScraperHelpers::getDivRepeat('some-class', $this->html, 1260);
        $this->assertSame([ 'Some class again' ], $div);

        $div = ScraperHelpers::getDivRepeat('some-class', $this->html, 1260, true);
        $this->assertSame([ '<div class="some-class">Some class again</div>' ], $div);

        $div = ScraperHelpers::getDivRepeat('<li>', $this->html, 1260);
        $this->assertSame([
            'list 1',
            'list 2',
            'list 3',
            'list 4',
        ], $div);

        $div = ScraperHelpers::getDivRepeat('zero-value', $this->html, 0, true);
        $this->assertSame([
            "<input name='zero-value' value='0'>",
            "<input name='zero-value-1' value='1'>",
        ], $div);
    }

    public function test_removeInlineCss()
    {
        $text = ScraperHelpers::removeInlineCss('Some text <style>h1 { font-size: 20px; }</style> more text ');
        $this->assertEquals('Some text  more text ', $text);

        $text = ScraperHelpers::removeInlineCss('Some text without inline style ');
        $this->assertEquals('Some text without inline style ', $text);
    }

    public function test_removeInlineJavascript()
    {
        $text = ScraperHelpers::removeInlineJavascript('Some text <script>var abc=1;</script> more text ');
        $this->assertEquals('Some text  more text ', $text);

        $text = ScraperHelpers::removeInlineJavascript('Some text without inline js');
        $this->assertEquals('Some text without inline js', $text);
    }

    public function test_removeHtmlComments()
    {
        $text = ScraperHelpers::removeHtmlComments('Some text <!-- HTML comments --> more text');
        $this->assertEquals('Some text  more text', $text);

        $text = ScraperHelpers::removeHtmlComments('Some text without comments');
        $this->assertEquals('Some text without comments', $text);
    }

    public function test_urlDecode()
    {
        $text = ScraperHelpers::urlDecode(urlencode('my=apples&are=green+and+red'));
        $this->assertEquals('my=apples&are=green+and+red', $text);

        $text = ScraperHelpers::urlDecode([
            urlencode('my=apples&are=green+and+red'),
            urlencode('my=oranges&are=orange'),
        ]);

        $this->assertSame([ 'my=apples&are=green+and+red', 'my=oranges&are=orange' ], $text);
    }

    public function test_removeEmpty()
    {
        $array = ScraperHelpers::removeEmpty([ '', 0, 1, false, true, 'abc', null ]);
        $this->assertSame([ 1, true, 'abc' ], $array);
    }

    public function test_shorterOf()
    {
        $text = ScraperHelpers::shorterOf([ 'abc', 'abcd', 'abcde' ]);
        $this->assertEquals('abc', $text);

        $text = ScraperHelpers::shorterOf([ 'abcdef', 'abcd', 'abcde' ]);
        $this->assertEquals('abcd', $text);

        $text = ScraperHelpers::shorterOf([ 'abcdef', 'abcd', 'abcde', '' ]);
        $this->assertEquals('', $text);

        $text = ScraperHelpers::shorterOf([ 'abcdef', 'abcd', 'abcde', 0 ]);
        $this->assertEquals(0, $text);

        $text = ScraperHelpers::shorterOf([ 'efgh', 'abcd', 'wxyz' ]);
        $this->assertEquals('abcd', $text);

        $text = ScraperHelpers::shorterOf([ 'efgh', 'abcd', 'wxyz', false ]);
        $this->assertEquals(false, $text);

        $text = ScraperHelpers::shorterOf([ 'efgh', 'abcd', 'wxyz', null ]);
        $this->assertEquals(null, $text);

        $text = ScraperHelpers::shorterOf([ 100, 3568, 25 ]);
        $this->assertEquals(25, $text);

        $text = ScraperHelpers::shorterOf([ 300, 3568, 259, -1786 ]);
        $this->assertEquals(259, $text);
    }

    public function test_removeBlankStrings()
    {
        $texts = ScraperHelpers::removeBlankStrings('string');
        $this->assertEquals('string', $texts);

        $texts = ScraperHelpers::removeBlankStrings([
            'string',
            '',
            null,
            false,
            0,
            123,
        ]);

        $this->assertSame([
            'string',
            0,
            123,
        ], $texts);
    }

    public function test_append()
    {
        $text = ScraperHelpers::append('suffix', '-text-');
        $this->assertSame('-text-suffix', $text);

        $text = ScraperHelpers::append('suffix', [
            '-first-',
            '-second-',
            '-third-',
        ]);

        $this->assertSame([
            '-first-suffix',
            '-second-suffix',
            '-third-suffix',
        ], $text);
    }

    public function test_prepend()
    {
        $text = ScraperHelpers::prepend('prefix', '-text-');
        $this->assertSame('prefix-text-', $text);

        $text = ScraperHelpers::prepend('prefix', [
            '-first-',
            '-second-',
            '-third-',
        ]);

        $this->assertSame([
            'prefix-first-',
            'prefix-second-',
            'prefix-third-',
        ], $text);
    }

    public function test_compressHtml()
    {
        $html = ScraperHelpers::compressHtml(" \n start\n<div class='some-class'>  stuff <br>  </div>\n\n  end ");
        $this->assertSame("start<div class='some-class'>stuff<br></div>end", $html);

        $html = ScraperHelpers::compressHtml([
            " \n start\n<div class='some-class'>  stuff <br>  </div>\n\n  end ",
            "<p><div class='some-class'>  stuff 111<br>  </div>\n\n",
        ]);
        $this->assertSame([
            "start<div class='some-class'>stuff<br></div>end",
            "<p><div class='some-class'>stuff 111<br></div>",
        ], $html);
    }

    public function test_getPostFields()
    {
        $postfields = ScraperHelpers::getPostFields($this->html, true);
        $this->assertSame([
            'input-name' => '12340',
            'input-name-again' => '5678',
            'zero-value' => '0',
            'zero-value-1' => '1',
            'textbox-value' => 'some-value',
            'checkbox-checked' => 1,
        ], $postfields);

        $postfields = ScraperHelpers::getPostFields($this->html, false);
        $this->assertSame([
            'input-name' => '12340',
            'input-name-again' => '5678',
            'zero-value' => '0',
            'zero-value-1' => '1',
            'textbox-value' => 'some-value',
        ], $postfields);
    }

    public function test_asNumeric()
    {
        $this->assertEquals(11, ScraperHelpers::asNumeric(11));
        $this->assertEquals(-22, ScraperHelpers::asNumeric(-22));
        $this->assertEquals(0.3, ScraperHelpers::asNumeric(0.3));
        $this->assertEquals(0, ScraperHelpers::asNumeric(0));
        $this->assertEquals(99, ScraperHelpers::asNumeric('99'));

        $this->assertEquals(0, ScraperHelpers::asNumeric('abc'));
        $this->assertEquals(0, ScraperHelpers::asNumeric([ 1, 2, 3 ]));
    }
}
